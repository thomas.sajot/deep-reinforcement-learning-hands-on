# Deep-Reinforcement-Learning-Hands-On-Second-Edition
Deep-Reinforcement-Learning-Hands-On-Second-Edition, published by Packt.

Fork from [the books repo](https://github.com/PacktPublishing/Deep-Reinforcement-Learning-Hands-On-Second-Edition).  
To Setup the fork from Github to Gitlab:
```shell script
git remote add upstream https://github.com/user/repo
git pull upstream master
git push origin master
```
